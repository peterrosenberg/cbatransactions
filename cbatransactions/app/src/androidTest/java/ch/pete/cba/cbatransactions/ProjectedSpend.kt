package ch.pete.cba.cbatransactions

import android.support.test.InstrumentationRegistry
import ch.pete.cba.cbatransactions.domain.Transaction
import ch.pete.cba.cbatransactions.repository.TransactionRepository
import ch.pete.cba.cbatransactions.repository.endpoints.TransactionResponse
import org.junit.Test
import timber.log.Timber
import java.util.*

/**
 *
 * Created by peter.rosenberg on 24/11/17.
 */
class ProjectedSpend {

    /**
     * very simplified variant to calculated potential spending by computing average.
     */
    @Test
    fun averageSpend() {
        val repository = TransactionRepository(InstrumentationRegistry.getTargetContext())
        val transactionsObservable = repository.fetchTransactions()

        val transactionResponse = transactionsObservable.blockingGet()
        val combinedTransactions = ArrayList<Transaction>()

        val transactionList: List<Transaction>? = transactionResponse.transactions
        if (transactionList != null) {
            combinedTransactions.addAll(transactionList)
        }

        val pendingTransactionList: List<Transaction>? = transactionResponse.pending
        if (pendingTransactionList != null) {
            pendingTransactionList.map { pendingTransaction ->
                pendingTransaction.pending = true
            }
            combinedTransactions.addAll(pendingTransactionList)
        }

        val startOfDateRangeCalendar = Calendar.getInstance()
        startOfDateRangeCalendar.add(Calendar.DAY_OF_YEAR, -720)
        val startOfDateRange = startOfDateRangeCalendar.time
        val transactionInTimeRange = combinedTransactions.filter { transaction ->
            val parsedDate = transaction.parsedDate
            parsedDate?.after(startOfDateRange) ?: false
        }

        var sum = 0.0
        var count = 0
        transactionInTimeRange.forEach { transaction ->
            val amount = transaction.amount
            if (amount != null && amount < 0) {
                count++
                sum += amount
            }
        }
        // is still negative
        sum = Math.abs(sum)

        val average = sum / count
        println("projected spend over the next two weeks: $$average")
    }
}
