package ch.pete.cba.cbatransactions.presenter

import android.app.Application
import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.content.Context
import ch.pete.cba.cbatransactions.domain.Atm
import ch.pete.cba.cbatransactions.domain.Transaction
import ch.pete.cba.cbatransactions.presenter.data.Cell
import ch.pete.cba.cbatransactions.presenter.data.TransactionCell
import ch.pete.cba.cbatransactions.repository.TransactionRepository
import ch.pete.cba.cbatransactions.repository.endpoints.TransactionResponse
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner

/**
 * Created by peter.rosenberg on 23/11/17.
 */
@RunWith(RobolectricTestRunner::class)
class TransactionsPresenterTest {
    companion object {
        private const val TEST_STRING = "TEST_STRING"
        private const val TEST_LAT = 123.0
        private const val TEST_LNG = 456.0
    }

    @Mock
    lateinit var titleObserver: Observer<String>
    @Mock
    lateinit var application: Application
    @Mock
    lateinit var repository: TransactionRepository
    @Mock
    lateinit var transactionCell: TransactionCell
    @Mock
    lateinit var cell: Cell
    @Mock
    lateinit var transactionCellContext: Context
    @Mock
    lateinit var cellContext: Context
    @Mock
    lateinit var transaction: Transaction
    @Mock
    lateinit var atm: Atm
    @Mock
    lateinit var atmLocation: Atm.Location
    @Mock
    lateinit var transactionResponse: TransactionResponse

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Rule
    @JvmField
    var mockitoRule: MockitoRule = MockitoJUnit.rule()

    private lateinit var presenter: TransactionsPresenter

    @Before
    fun initMocks() {
        whenever(application.getString(anyInt())).thenReturn(TEST_STRING)
        presenter = TransactionsPresenter(application)
        presenter.repository = repository
    }

    @Test
    fun loadData() {
        whenever(repository.fetchTransactions()).thenReturn(Single.just(transactionResponse))
        presenter.title.observeForever(titleObserver)

        presenter.loadData()

        verify<Observer<String>>(titleObserver, times(1)).onChanged(TEST_STRING)
    }

    @Test
    fun onLocationClick() {
        whenever(transactionCell.transaction).thenReturn(transaction)
        whenever(transaction.atm).thenReturn(atm)
        whenever(atm.location).thenReturn(atmLocation)
        whenever(atmLocation.lat).thenReturn(TEST_LAT)
        whenever(atmLocation.lng).thenReturn(TEST_LNG)

        presenter.onLocationClicked(transactionCell, transactionCellContext)
        verify(transactionCellContext, times(1)).startActivity(ArgumentMatchers.any())

        presenter.onLocationClicked(cell, cellContext)
        verify(cellContext, times(0)).startActivity(ArgumentMatchers.any())
    }

    // TODO many more tests
}
