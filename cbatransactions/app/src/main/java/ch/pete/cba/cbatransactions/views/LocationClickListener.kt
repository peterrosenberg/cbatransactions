package ch.pete.cba.cbatransactions.views

import ch.pete.cba.cbatransactions.presenter.data.Cell

/**
 * Created by peter.rosenberg on 23/11/17.
 */
interface LocationClickListener {
    fun onLocationClicked(cell: Cell)
}
