package ch.pete.cba.cbatransactions.views

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import ch.pete.cba.cbatransactions.R
import ch.pete.cba.cbatransactions.presenter.data.Cell
import ch.pete.cba.cbatransactions.presenter.data.DateCell
import ch.pete.cba.cbatransactions.presenter.data.HeaderCell
import ch.pete.cba.cbatransactions.presenter.data.TransactionCell

/**
 * Adapter for the transaction recycler view.
 * Created by peter.rosenberg on 23/11/17.
 */
class TransactionViewAdapter(private val cells: List<Cell>,
                             private val locationClickListener: LocationClickListener)
    : RecyclerView.Adapter<TransactionViewHolder>() {

    companion object {
        private const val CELL_TYPE_HEADER = 0
        private const val CELL_TYPE_TRANSACTION = 1
        private const val CELL_TYPE_DATE = 2
    }

    override fun getItemViewType(position: Int): Int {
        return when (cells[position]) {
            is HeaderCell -> {
                CELL_TYPE_HEADER
            }
            is TransactionCell -> {
                CELL_TYPE_TRANSACTION
            }
            is DateCell -> {
                CELL_TYPE_DATE
            }
            else -> {
                throw IllegalArgumentException("Wrong cell found")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        when (viewType) {
            CELL_TYPE_HEADER -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.header_cell, parent, false)
                val holder = TransactionViewHolder(itemView, locationClickListener)
                holder.tvAccountName = itemView.findViewById(R.id.tvAccountName)
                holder.tvAccountNumber = itemView.findViewById(R.id.tvAccountNumber)
                holder.tvAvailableFunds = itemView.findViewById(R.id.tvAvailableFunds)
                holder.tvAccountBalance = itemView.findViewById(R.id.tvAccountBalance)
                return holder
            }
            CELL_TYPE_TRANSACTION -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.transaction_cell, parent, false)
                val holder = TransactionViewHolder(itemView, locationClickListener)
                holder.ivLocation = itemView.findViewById(R.id.ivLocation)
                holder.tvPending = itemView.findViewById(R.id.tvPending)
                holder.tvDescription = itemView.findViewById(R.id.tvDescription)
                holder.tvAmount = itemView.findViewById(R.id.tvAmount)
                return holder
            }
            CELL_TYPE_DATE -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.date_cell, parent, false)
                val holder = TransactionViewHolder(itemView, locationClickListener)
                holder.tvDate = itemView.findViewById(R.id.tvDate)
                holder.tvDays = itemView.findViewById(R.id.tvDays)
                return holder
            }
            else -> {
                throw IllegalArgumentException("Wrong cell type")
            }
        }
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val cell = cells[position]
        holder.cell = cell

        when (cell) {
            is HeaderCell -> {
                holder.tvAccountName?.text = cell.account.accountName
                holder.tvAccountNumber?.text = cell.account.accountNumber
                holder.tvAvailableFunds?.text = convertAmount(cell.account.available, holder.tvAvailableFunds?.context)
                holder.tvAccountBalance?.text = convertAmount(cell.account.balance, holder.tvAvailableFunds?.context)
            }
            is TransactionCell -> {
                if (cell.transaction.pending) {
                    holder.tvPending?.visibility = View.VISIBLE
                } else {
                    holder.tvPending?.visibility = View.GONE
                }

                if (cell.transaction.atmId != null) {
                    holder.ivLocation?.visibility = View.VISIBLE
                    holder.itemView.setOnClickListener(holder.clickListener)
                } else {
                    holder.ivLocation?.visibility = View.GONE
                    holder.itemView.setOnClickListener(null)
                }

                val descriptionHtml = cell.transaction.description
                @Suppress("DEPRECATION")
                val description = Html.fromHtml(descriptionHtml)
                holder.tvDescription?.text = description

                val amountStr = convertAmount(cell.transaction.amount, holder.tvAmount?.context)
                if (amountStr != null) {
                    holder.tvAmount?.text = amountStr
                }
            }
            is DateCell -> {
                holder.tvDate?.text = cell.dateStr
                holder.tvDays?.text = cell.daysStr
            }
        }
    }

    private fun convertAmount(amount: Double?, context: Context?): String? {
        val dollarStr = context?.getString(R.string.amount)
        if (dollarStr != null) {
            var minusSign = ""
            var convertedAmount = amount
            if (convertedAmount != null && convertedAmount < 0.0) {
                minusSign = "-"
                convertedAmount *= -1
            }
            return String.format(dollarStr, minusSign, convertedAmount)
        }
        return null
    }

    override fun getItemCount(): Int {
        return cells.size
    }
}

class TransactionViewHolder(itemView: View, locationClickListener: LocationClickListener) : RecyclerView.ViewHolder(itemView) {
    var cell: Cell? = null

    var tvAccountName: TextView? = null
    var tvAccountNumber: TextView? = null
    var tvAvailableFunds: TextView? = null
    var tvAccountBalance: TextView? = null

    var ivLocation: ImageView? = null
    var tvPending: TextView? = null
    var tvDescription: TextView? = null
    var tvAmount: TextView? = null

    var tvDate: TextView? = null
    var tvDays: TextView? = null

    val clickListener = View.OnClickListener {
        val cellLocal = cell
        if (cellLocal != null) {
            locationClickListener.onLocationClicked(cellLocal)
        }
    }

    override fun toString(): String {
        // add the class name as the super.toString() method misses it
        return " " + javaClass.name + ": " + super.toString()
    }
}
