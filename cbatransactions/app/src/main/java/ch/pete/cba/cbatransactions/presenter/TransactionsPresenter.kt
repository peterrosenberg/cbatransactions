package ch.pete.cba.cbatransactions.presenter

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.content.Intent
import ch.pete.cba.cbatransactions.R
import ch.pete.cba.cbatransactions.domain.Atm
import ch.pete.cba.cbatransactions.domain.Transaction
import ch.pete.cba.cbatransactions.presenter.data.Cell
import ch.pete.cba.cbatransactions.presenter.data.DateCell
import ch.pete.cba.cbatransactions.presenter.data.HeaderCell
import ch.pete.cba.cbatransactions.presenter.data.TransactionCell
import ch.pete.cba.cbatransactions.repository.TransactionRepository
import ch.pete.cba.cbatransactions.repository.endpoints.TransactionResponse
import ch.pete.cba.cbatransactions.views.FindUsActivity
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 * Implements the logic of the transactions page. Logic is placed here to make it testable and
 * independent of the view.
 * Created by peter.rosenberg on 23/11/17.
 */
class TransactionsPresenter(application: Application) : AndroidViewModel(application) {
    companion object {
        private const val ARG_LAT = "arg_lat"
        private const val ARG_LNG = "arg_lng"
        private const val DISPLAY_DATE_FORMAT = "dd MMM YYYY"
    }

    val title = MutableLiveData<String>()
    val transactions = MutableLiveData<List<Cell>>()
    var repository: TransactionRepository? = null

    /**
     * trigger to load data/fetch from network
     */
    fun loadData() {
        val application: Application = getApplication()
        title.value = application.getString(R.string.transactions_fragment_account_details)

        fetchTransactions()
    }

    /**
     * does the network call using RxJava and Retrofit and then update transactions variable
     */
    @SuppressLint("CheckResult")
    private fun fetchTransactions() {
        repository?.fetchTransactions()
                ?.observeOn(Schedulers.io())
                ?.subscribe(
                        { transactionResponse ->
                            processResponse(transactionResponse)
                        },
                        { throwable ->
                            // TODO notify the user
                            Timber.e("error: %s", throwable)
                        }
                )
    }

    private fun processResponse(transactionResponse: TransactionResponse) {
        val combinedTransactions = ArrayList<Transaction>()

        val transactionList: List<Transaction>? = transactionResponse.transactions
        if (transactionList != null) {
            combinedTransactions.addAll(transactionList)
        }

        val pendingTransactionList: List<Transaction>? = transactionResponse.pending
        if (pendingTransactionList != null) {
            pendingTransactionList.map { pendingTransaction ->
                pendingTransaction.pending = true
            }
            combinedTransactions.addAll(pendingTransactionList)
        }

        addAtms(combinedTransactions, transactionResponse)

        combinedTransactions.sortByDescending { transaction ->
            transaction.parsedDate
        }

        val newCells = ArrayList<Cell>()
        val todayDate = Date()
        var currentDate: Date? = null
        for (transaction in combinedTransactions) {
            if (currentDate != transaction.parsedDate) {
                val parsedDate = transaction.parsedDate
                if (parsedDate != null) {
                    val formatter = SimpleDateFormat(DISPLAY_DATE_FORMAT, Locale.US)
                    var dateStr = formatter.format(parsedDate).toUpperCase()
                    // workaround for dot after month bug
                    dateStr = dateStr.replace(".", "")

                    val diffInDays = TimeUnit.MILLISECONDS.toDays(todayDate.time - parsedDate.time)
                    val context: Context = getApplication()
                    val daysStr = context.resources.getQuantityString(R.plurals.days_ago,
                            diffInDays.toInt(), diffInDays)
                    newCells.add(DateCell(dateStr, daysStr))
                }
            }
            currentDate = transaction.parsedDate
            newCells.add(TransactionCell(transaction))
        }

        val account = transactionResponse.account
        if (account != null) {
            newCells.add(0, HeaderCell(account))
        }

        transactions.postValue(newCells)
    }

    /**
     * at the Atm objects to the transaction objects if the transaction is related
     * to an Atm.
     */
    private fun addAtms(combinedTransactions: List<Transaction>, transactionResponse: TransactionResponse) {
        val mappedAtms = HashMap<String, Atm>()
        transactionResponse.atms?.map { atm ->
            val atmId = atm.id
            if (atmId != null) {
                mappedAtms[atmId] = atm
            }
        }

        combinedTransactions.forEach { transaction ->
            val atmId = transaction.atmId
            if (atmId != null) {
                val atm = mappedAtms[atmId]
                transaction.atm = atm
            }
        }
    }

    /**
     * Callback from the view when the location cell is clicked
     * @param cell that was clicked
     * @param context of the view to be used to action the event
     */
    fun onLocationClicked(cell: Cell, context: Context?) {
        if (cell is TransactionCell) {
            val intent = Intent(context, FindUsActivity::class.java)
            intent.putExtra(ARG_LAT, cell.transaction.atm?.location?.lat)
            intent.putExtra(ARG_LNG, cell.transaction.atm?.location?.lng)
            context?.startActivity(intent)
        }
    }
}
