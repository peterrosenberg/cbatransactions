package ch.pete.cba.cbatransactions.views

import android.os.Bundle
import ch.pete.cba.cbatransactions.R
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_find_us.*

/**
 * Activity to show Google Maps with the location of the ATM.
 */
class FindUsActivity : AppCompatActivity(), OnMapReadyCallback {
    companion object {
        const val ARG_LAT = "arg_lat"
        const val ARG_LNG = "arg_lng"

        /**
         * 0 lowest zoom (whole world)
         * 19 highest zoom (individual buildings, if available)
         */
        private const val ZOOM_LEVEL = 17f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_us)
        setSupportActionBar(find_us_toolbar)
        supportActionBar?.title = null

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        vgUpNavigation.setOnClickListener { finish() }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val argLat = intent.getDoubleExtra(ARG_LAT, Double.MAX_VALUE)
        val argLng = intent.getDoubleExtra(ARG_LNG, Double.MAX_VALUE)
        if (argLat != Double.MAX_VALUE && argLng != Double.MAX_VALUE) {
            val location = LatLng(argLat, argLng)
            val icon: BitmapDescriptor = BitmapDescriptorFactory
                    .fromResource(R.drawable.marker_atm_commbank)
            googleMap.addMarker(MarkerOptions().position(location).icon(icon))
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(location))
            googleMap.moveCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL))
        }
    }
}
