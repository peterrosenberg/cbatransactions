package ch.pete.cba.cbatransactions.domain

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by peter.rosenberg on 23/11/17.
 */
open class Transaction {
    companion object {
        private const val DATE_PARSE_PATTERN = "dd/MM/yyyy"
    }

    var id: String? = null
        private set
    var description: String? = null
        private set
    var effectiveDate: String? = null
        private set
    var amount: Double? = null
        private set
    var atmId: String? = null
        private set

    // local values
    open var atm: Atm? = null
    /**
     * cache the parsed date for performance reasons
     */
    var parsedDate: Date? = null
        get() {
            if (field == null) {
                val parser = SimpleDateFormat(DATE_PARSE_PATTERN, Locale.US)
                field = parser.parse(effectiveDate)

            }
            return field
        }
    /**
     * mark the transaction as pending/not pending to not need to use multiple lists
     */
    var pending: Boolean = false
}
