package ch.pete.cba.cbatransactions.presenter.data

/**
 * Cell that represents the date group
 * Created by peter.rosenberg on 23/11/17.
 */
class DateCell(var dateStr: String, var daysStr: String) : Cell()
