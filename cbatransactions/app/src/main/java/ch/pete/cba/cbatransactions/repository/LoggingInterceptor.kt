package ch.pete.cba.cbatransactions.repository

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.ResponseBody
import okio.Buffer
import timber.log.Timber
import java.io.IOException
import java.net.URLDecoder

/**
 * Prints request and response URL and data to console.
 * For debug purpose only.
 * Created by peter.rosenberg on 23/11/2017.
 */
class LoggingInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val request = chain.request()
        val t1 = System.currentTimeMillis()
        if (LOG_REQUEST) {
            val requestLog = StringBuilder()
            requestLog.append("Sending request")
            requestLog.append("\n")
            requestLog.append(request.url())
            requestLog.append("\n")
            if (chain.connection() != null) {
                requestLog.append("on ")
                requestLog.append(chain.connection())
                requestLog.append("\n")
            }
            requestLog.append(request.headers())

            if (request.method().compareTo("post", ignoreCase = true) == 0) {
                requestLog.append("\n")
                requestLog.append(bodyToString(request))
            }
            Timber.d(requestLog.toString())
        }
        val response = chain.proceed(request)
        val t2 = System.currentTimeMillis()
        val bodyString = response.body()!!.string()

        if (LOG_RESPONSE) {
            val responseLog = StringBuilder()
            responseLog.append("Received response for")
            responseLog.append("\n")
            responseLog.append(URLDecoder.decode(response.request().url().toString(), "UTF-8"))
            responseLog.append("\n")
            responseLog.append("in ")
            responseLog.append(t2 - t1)
            responseLog.append("ms")
            responseLog.append("\n")
            if (LOG_RESPONSE_HEADERS) {
                responseLog.append(response.headers())
                responseLog.append("\n")
            }
            responseLog.append(bodyString)

            Timber.d(responseLog.toString())
        }

        return response.newBuilder()
                .body(ResponseBody.create(response.body()!!.contentType(), bodyString))
                .build()
    }

    companion object {
        private const val LOG_REQUEST = true
        private const val LOG_RESPONSE = true
        private const val LOG_RESPONSE_HEADERS = false

        fun bodyToString(request: Request): String {
            return try {
                val copy = request.newBuilder().build()
                val buffer = Buffer()
                copy.body()!!.writeTo(buffer)
                buffer.readUtf8()
            } catch (e: IOException) {
                "Could not convert body to string"
            }
        }
    }
}
