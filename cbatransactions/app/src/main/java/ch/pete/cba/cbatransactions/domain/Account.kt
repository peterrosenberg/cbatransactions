package ch.pete.cba.cbatransactions.domain

/**
 * Created by peter.rosenberg on 23/11/17.
 */
class Account {
    var accountName: String? = null
        private set
    var accountNumber: String? = null
        private set
    /**
     * available balance on the account
     */
    var available: Double? = null
        private set
    /**
     * account balance
     */
    var balance: Double? = null
        private set
}
