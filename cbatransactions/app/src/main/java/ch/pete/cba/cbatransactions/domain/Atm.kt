package ch.pete.cba.cbatransactions.domain

/**
 * Created by peter.rosenberg on 23/11/17.
 */
open class Atm {
    var id: String? = null
        private set
    var name: String? = null
        private set
    var address: String? = null
        private set
    open var location: Location? = null

    open class Location {
        open var lat: Double? = null
        open var lng: Double? = null
    }
}
