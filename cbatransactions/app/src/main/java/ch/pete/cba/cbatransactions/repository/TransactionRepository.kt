package ch.pete.cba.cbatransactions.repository

import android.content.Context
import ch.pete.cba.cbatransactions.BuildConfig
import ch.pete.cba.cbatransactions.R
import ch.pete.cba.cbatransactions.repository.endpoints.TransactionEndpoints
import ch.pete.cba.cbatransactions.repository.endpoints.TransactionResponse
import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Provides access to the transaction data.
 * Created by peter.rosenberg on 23/11/17.
 */
open class TransactionRepository(context: Context) {
    private val transactionEndpoints: TransactionEndpoints

    init {
        val gson = GsonBuilder().create()

        val builder = OkHttpClient.Builder()
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(LoggingInterceptor())
        }
        val client = builder.build()

        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .baseUrl(context.getString(R.string.BASE_URL))
                .build()
        transactionEndpoints = retrofit.create(TransactionEndpoints::class.java)
    }

    fun fetchTransactions(): Single<TransactionResponse> {
        return transactionEndpoints.transactions
    }
}
