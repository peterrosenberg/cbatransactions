package ch.pete.cba.cbatransactions.presenter.data

import ch.pete.cba.cbatransactions.domain.Transaction

/**
 * Cell to display each individual transaction. Used for cleared and pending transactions.
 * Created by peter.rosenberg on 23/11/17.
 */
open class TransactionCell(open var transaction: Transaction) : Cell()
