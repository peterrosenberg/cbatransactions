package ch.pete.cba.cbatransactions.presenter.data

import ch.pete.cba.cbatransactions.domain.Account


/**
 * One cell on top to show the account info
 * Created by peter.rosenberg on 23/11/17.
 */
class HeaderCell(var account: Account) : Cell()
