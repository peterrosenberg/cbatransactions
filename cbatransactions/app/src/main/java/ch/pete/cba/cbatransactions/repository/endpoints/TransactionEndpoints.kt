package ch.pete.cba.cbatransactions.repository.endpoints

import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by peter.rosenberg on 23/11/17.
 */
interface TransactionEndpoints {

    /**
     * @return the transactions items
     */
    @get:GET("/s/tewg9b71x0wrou9/data.json?dl=1")
    val transactions: Single<TransactionResponse>
}
