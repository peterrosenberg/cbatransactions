package ch.pete.cba.cbatransactions.views

/**
 * Marks activities where the title can be suggested by fragments.
 * Created by peter.rosenberg on 23/11/17.
 */
interface TitleSupportActivity {
    fun setTitle(title: String?)
}
