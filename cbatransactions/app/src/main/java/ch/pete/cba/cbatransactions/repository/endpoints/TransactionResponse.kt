package ch.pete.cba.cbatransactions.repository.endpoints

import ch.pete.cba.cbatransactions.domain.Account
import ch.pete.cba.cbatransactions.domain.Atm
import ch.pete.cba.cbatransactions.domain.Transaction


/**
 * Wraps the response.
 * Created by peter.rosenberg on 23/11/17.
 */
class TransactionResponse {
    var account: Account? = null
        private set
    var transactions: List<Transaction>? = null
        private set
    var pending: List<Transaction>? = null
        private set
    var atms: List<Atm>? = null
        private set
}
