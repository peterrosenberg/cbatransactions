package ch.pete.cba.cbatransactions.views

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import ch.pete.cba.cbatransactions.R
import kotlinx.android.synthetic.main.main_activity.*

/**
 * First activity to show, contains the transactions fragment.
 * Created by peter.rosenberg on 23/11/17.
 */
class MainActivity : AppCompatActivity(), TitleSupportActivity {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setSupportActionBar(main_toolbar)
        supportActionBar?.title = null

        if (savedInstanceState == null) {
            val ft = supportFragmentManager.beginTransaction()
            val fragment: Fragment = TransactionsFragment()

            ft.add(R.id.main_activity_container, fragment, fragment.javaClass.name)
            ft.commit()
        }
    }

    override fun setTitle(title: String?) {
        main_toolbar_title.text = title
    }
}
