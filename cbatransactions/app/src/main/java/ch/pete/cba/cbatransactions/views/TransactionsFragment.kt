package ch.pete.cba.cbatransactions.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ch.pete.cba.cbatransactions.R
import ch.pete.cba.cbatransactions.presenter.TransactionsPresenter
import ch.pete.cba.cbatransactions.presenter.data.Cell
import ch.pete.cba.cbatransactions.repository.TransactionRepository
import kotlinx.android.synthetic.main.transactions_fragment.*

/**
 * View to display the transaction list. As dump as possible to keep the logic out and testable.
 * Created by peter.rosenberg on 23/11/17.
 */
class TransactionsFragment : Fragment() {
    private lateinit var transactionPresenter: TransactionsPresenter
    private var adapter: TransactionViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val fragmentActivity = activity!! // activity is not null in onCreate()
        transactionPresenter = ViewModelProviders.of(fragmentActivity).get(TransactionsPresenter::class.java)
        transactionPresenter.repository = TransactionRepository(fragmentActivity)
        initListeners()
        transactionPresenter.loadData()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.transactions_fragment, container, false)
    }

    private fun setTitle(title: String?) {
        val attachedActivity = activity
        if (attachedActivity is TitleSupportActivity) {
            attachedActivity.setTitle(title)
        }
    }

    private fun initListeners() {
        transactionPresenter.title.observe(this, Observer { setTitle(it) })
        transactionPresenter.transactions.observe(this, Observer { cells ->

            val locationClickListener = object : LocationClickListener {
                override fun onLocationClicked(cell: Cell) {
                    transactionPresenter.onLocationClicked(cell, activity)
                }
            }
            adapter = if (cells != null) {
                TransactionViewAdapter(cells, locationClickListener)
            } else {
                TransactionViewAdapter(ArrayList(), locationClickListener)
            }
            transactions_recycler_view.layoutManager = LinearLayoutManager(context)
            transactions_recycler_view.adapter = adapter
        })
    }
}
