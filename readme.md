## Exercise Notes
- See class ProjectedSpend regarding projected spend
- The tests do not reach the desired code coverage due to the limited time

The app has room for improvements like

- loading progress bar
- background sync (e.g. with SyncAdapter)
- remove ripple when cell not clickable
- more tests
- match UI design more closely with the screen shots
